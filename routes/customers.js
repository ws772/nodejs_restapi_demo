var express = require('express');
var app = express();
var router = express.Router();
var connection  = require('express-myconnection'); 
var mysql = require('mysql');

app.use(
    
    connection(mysql,{
        
        host: 'localhost',
        user: 'root',
        password : '',
        port : 3306, 
        database:'nodejs_test'
    },'request')
);

//  router listing. 
router.post('/customers/edit/:id',customers.save_edit);
router.post('/customers/add', customers.save);
router.get('/customers/add', customers.add);
router.get('/customers/edit/:id', customers.edit); 
router.get('/customers/delete/:id', customers.delete_customer);
router.get('/customers', customers.list);

module.exports = router;